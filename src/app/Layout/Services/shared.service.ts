import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public PendingNotListner = new BehaviorSubject<any>('');

  public sharedNotification(item) {
    this.PendingNotListner.next(item);
  }

  public jwt() {
    if (localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser'))) {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
        })
      }
    }
  }
}