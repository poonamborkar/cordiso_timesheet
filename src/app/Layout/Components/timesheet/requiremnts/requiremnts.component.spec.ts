import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiremntsComponent } from './requiremnts.component';

describe('RequiremntsComponent', () => {
  let component: RequiremntsComponent;
  let fixture: ComponentFixture<RequiremntsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiremntsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiremntsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
