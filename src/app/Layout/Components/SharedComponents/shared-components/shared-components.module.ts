import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangePickerComponent } from 'src/app/Layout/Components/SharedComponents/date-range-picker/date-range-picker.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from 'src/app/material-module';
import { FileUploadComponent } from '../file-upload/file-upload.component';

@NgModule({
  declarations: [DateRangePickerComponent,FileUploadComponent],
  imports: [
    NgxDaterangepickerMd,
    CommonModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    MaterialModule
  ],
  exports: [
    DateRangePickerComponent ,FileUploadComponent
  ]
  
})
export class SharedComponentsModule { }
